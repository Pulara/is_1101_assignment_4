#include <stdio.h>
int main() {
    int n;
    printf("Enter an integer: ");
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) {
        if(i==1)
        printf("%d time  %d = %d \n", i, n, n * i);
        else
        printf("%d times %d = %d \n", i, n, n * i);
    }
    return 0;
}

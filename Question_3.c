#include <stdio.h>

int
main ()
{
  int num;
  printf ("Enter a number to reverse: ");
  scanf ("%d", &num);
  int revNum, rem;
  while (num != 0)
    {
      rem = num % 10;
      revNum = revNum * 10 + rem;
      num /= 10;
    }
  printf ("Reversed number is: %d", revNum);
  return 0;
}

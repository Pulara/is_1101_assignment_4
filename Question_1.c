#include <stdio.h>

int main ()
{
  int n, sum = 0;
  printf ("Enter a positive integer: ");
  scanf ("%d", &n);
  while (n > 0)
    {
      sum += n;
      scanf ("%d", &n);
    }
  printf ("Sum = %d ", sum);
  return 0;
}

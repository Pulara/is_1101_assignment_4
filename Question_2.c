#include <stdio.h>

int
main ()
{
  int n, half;
  printf ("Enter a number to check: ");
  scanf ("%d", &n);
  if (n > 1)
    {
      half = n / 2;
      int i = 2, rem = n%2;
      while (rem != 0 && i <= half)
	{

	  rem = n % i;
	  i++;

	}
      if (i > half)
	printf ("Entered number is A PRIME number.");
      else
	printf ("Entered number is NOT A PRIME number.");
    }
  else
    printf ("Entered number is NOT A PRIME number.");
  return 0;
}
